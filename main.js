/**
 * Created by jk_87 on 28/3/2016.
 */
// Made by tjk
var express = require("express")
var app = express();

//time assignment
app.get("/",
    function(req, res) {

        //Status
        res.status(202);
        // MIME TYPE
        res.type("text/plain");
        res.send("The current time is " + (new Date()));
    }
);

// Hello assignment
app.get("/hello",
    function(req, res) {

        //Status
        res.status(202);
        // MIME TYPE
        res.type("text/html");

        res.send("<h1> HELLO </h1>");
    }
);


//3000 - port number
app.listen(3000, function () {
    console.info("Application started on port 3000");
});

